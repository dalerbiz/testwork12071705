<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Something;

class SomethingController extends Controller
{
    public function index()
    {
        $somethings = Something::all()->toArray();
        return array_reverse($somethings);
    }

    public function store(Request $request)
    {
        $something = new Something([
            'name' => $request->input('name'),
            'content' => $request->input('content')
        ]);
        $something->save();

        return response()->json('Создали что то!');
    }

    public function show($id)
    {
        $something = Something::find($id);
        return response()->json($something);
    }

    public function update($id, Request $request)
    {
        $something = Something::find($id);
        $something->update($request->all());

        return response()->json('Обновили что то!');
    }

    public function destroy($id)
    {
        $something = Something::find($id);
        $something->delete();

        return response()->json('Удалили что то!');
    }
}
