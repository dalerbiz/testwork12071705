import AllSomething from './components/AllSomething.vue';
import CreateSomething from './components/CreateSomething.vue';
import EditSomething from './components/EditSomething.vue';

export const routes = [
    {
        name: 'home',
        path: '/',
        component: AllSomething
    },
    {
        name: 'create',
        path: '/create',
        component: CreateSomething
    },
    {
        name: 'edit',
        path: '/edit/:id',
        component: EditSomething
    }
];
